/** @type {import("@trivago/prettier-plugin-sort-imports").PrettierConfig} */
module.exports = {
    semi: false,
    singleQuote: true,
    tabWidth: 4,
    printWidth: 100,
    importOrder: [
        '^node:', // Node.js built-ins
        '^[a-z@]', // npm dependencies
        '^\\.', // Local files
    ],
    importOrderCaseInsensitive: true,
    importOrderSeparation: true,
    importOrderSortSpecifiers: true,
    plugins: ['@trivago/prettier-plugin-sort-imports'],
}
