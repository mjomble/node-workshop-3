import type { UserSelect } from '../server/types'
import type { View } from './types'

export const loadUsers = async (view: View) => {
    const { state, update } = view
    const res = await fetch('/users')

    if (res.ok) {
        state.users = (await res.json()) as UserSelect[]
        update()
    } else {
        alert('Failed to load users')
    }
}

export const addUser = async (view: View) => {
    const { state, update } = view
    const { userForm } = state

    const res = await fetch('/users', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            first_name: userForm.firstName,
            last_name: userForm.lastName,
        }),
    })

    if (res.ok) {
        const resBody = (await res.json()) as { id: number }
        console.log('Added user with id', resBody.id)

        userForm.firstName = ''
        userForm.lastName = ''
        userForm.errors = {}
        update()

        await loadUsers(view)
    } else {
        alert('Failed to add user')
    }
}

export const deleteUser = async (view: View, userId: number) => {
    const res = await fetch(`/users/${userId}`, {
        method: 'DELETE',
    })

    if (res.ok) {
        await loadUsers(view)
    } else {
        alert('Failed to remove user')
    }
}
