import { h } from 'preact'

export interface ButtonProps {
    text?: string
    onClick?: () => void
    disabled?: boolean
    class?: string
}

export const Button = (props: ButtonProps) => (
    <button class={'button ' + props.class} onClick={props.onClick} disabled={props.disabled}>
        {props.text}
    </button>
)
