import { h } from 'preact'

import { UserForm, type UserFormProps } from './UserForm'
import { UserList, type UserListProps } from './UserList'

export interface RootProps {
    userForm: UserFormProps
    userList: UserListProps
}

export const Root = (props: RootProps) => (
    <div class="root">
        <UserForm {...props.userForm} />
        <UserList {...props.userList} />
    </div>
)
