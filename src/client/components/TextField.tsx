import { h } from 'preact'

export interface TextFieldProps {
    label: string
    value: string
    onChange: (value: string) => void
    error?: string
}

export const TextField = (props: TextFieldProps) => (
    <div class="textfield">
        <label>
            <div class="textfield__label">{props.label}</div>
            <div>
                <input
                    type="text"
                    class="textfield__input"
                    value={props.value}
                    onInput={(event) => props.onChange(event.currentTarget.value)}
                />
            </div>
        </label>
        <div class="textfield__error">{props.error}</div>
    </div>
)
