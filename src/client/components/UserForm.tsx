import { h } from 'preact'

import { Button, type ButtonProps } from './Button'
import { TextField, type TextFieldProps } from './TextField'

export interface UserFormProps {
    firstName: TextFieldProps
    lastName: TextFieldProps
    submitButton: ButtonProps
}

export const UserForm = (props: UserFormProps) => (
    <div class="user-form">
        <div>
            <TextField {...props.firstName} />
        </div>
        <div>
            <TextField {...props.lastName} />
        </div>
        <div>
            <Button {...props.submitButton} />
        </div>
    </div>
)
