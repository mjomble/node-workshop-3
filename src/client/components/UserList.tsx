import { Fragment, h } from 'preact'

import { Button, type ButtonProps } from './Button'

export interface UserListProps {
    users: UserProps[]
}

export interface UserProps {
    key: number
    name: string
    removeButton: ButtonProps
}

export const UserList = (props: UserListProps) => (
    <>
        <h2>Kasutajad</h2>
        {props.users.map((user) => (
            <User {...user} />
        ))}
    </>
)

const User = (props: UserProps) => (
    <div class="user">
        <Button class="user__remove-button" {...props.removeButton} />
        <span>{props.name}</span>
    </div>
)
