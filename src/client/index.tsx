import { h, render } from 'preact'

import { loadUsers } from './actions'
import { Root } from './components/Root'
import { getRootProps } from './props'
import type { State, View } from './types'

window.addEventListener('load', async () => {
    const container = document.getElementById('root')!

    const state: State = {
        users: [],
        userForm: {
            firstName: '',
            lastName: '',
            errors: {},
        },
    }

    const view: View = {
        state,
        update: () => {
            const rootProps = getRootProps(view)
            render(<Root {...rootProps} />, container)
        },
    }

    view.update() // First render

    await loadUsers(view)
})
