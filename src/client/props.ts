import type { UserSelect } from '../server/types'
import { addUser, deleteUser } from './actions'
import type { RootProps } from './components/Root'
import type { UserFormProps } from './components/UserForm'
import type { UserListProps, UserProps } from './components/UserList'
import type { View } from './types'

export const getRootProps = (view: View): RootProps => ({
    userForm: getUserFormProps(view),
    userList: getUserListProps(view),
})

const getUserFormProps = (view: View): UserFormProps => {
    const { state, update } = view
    const { userForm } = state

    return {
        firstName: {
            label: 'Eesnimi',
            value: userForm.firstName,
            onChange: (value) => {
                userForm.firstName = value
                update()
            },
            error: userForm.errors.first_name,
        },
        lastName: {
            label: 'Perekonnanimi',
            value: userForm.lastName,
            onChange: (value) => {
                userForm.lastName = value
                update()
            },
            error: userForm.errors.last_name,
        },
        submitButton: {
            text: 'Lisa',
            onClick: async () => addUser(view),
        },
    }
}

const getUserListProps = (view: View): UserListProps => {
    const { state } = view

    return {
        users: state.users.map((user) => getUserProps(view, user)),
    }
}

const getUserProps = (view: View, user: UserSelect): UserProps => ({
    key: user.id,
    name: `${user.first_name} ${user.last_name}`,
    removeButton: {
        text: '×',
        onClick: async () => deleteUser(view, user.id),
    },
})
