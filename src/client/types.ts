import type { UserSelect } from '../server/types'

export interface State {
    users: UserSelect[]
    userForm: {
        firstName: string
        lastName: string
        errors: Record<string, string | undefined>
    }
}

export interface View {
    state: State
    update: () => void
}
