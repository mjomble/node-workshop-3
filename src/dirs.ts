import path from 'node:path'

const srcDir = __dirname

export const srcClientDir = path.resolve(srcDir, 'client')
export const srcClientStaticDir = path.resolve(srcClientDir, 'static')
