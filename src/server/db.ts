import SQLite from 'better-sqlite3'
import { Kysely, SqliteDialect } from 'kysely'

import type { Database } from './types'

export const initDatabase = async () => {
    const sqlite = new SQLite(':memory:')
    const dialect = new SqliteDialect({ database: sqlite })
    const db = new Kysely<Database>({ dialect })

    sqlite
        .prepare(
            `CREATE TABLE users (
                id INTEGER PRIMARY KEY,
                first_name TEXT,
                last_name TEXT NULLABLE
            )`,
        )
        .run()

    await db
        .insertInto('users')
        .values([
            { first_name: 'John', last_name: 'Doe' },
            { first_name: 'Jane', last_name: 'Doe' },
        ])
        .execute()

    return db
}
