import type { Kysely } from 'kysely'
import send from 'send'

import { srcClientStaticDir } from '../dirs'
import type { Database, RequestHandler, UserInsert } from './types'
import { createJsonResponse } from './utils'

export const createHandlers = (db: Kysely<Database>): RequestHandler[] => [
    {
        method: 'GET',
        pathPattern: '/users',
        handle: async () => {
            const users = await db.selectFrom('users').selectAll().execute()
            return createJsonResponse(users)
        },
    },
    {
        method: 'GET',
        pathPattern: '/users/:id',
        handle: async (params) => {
            const { pathParams } = params
            const user = await db
                .selectFrom('users')
                .selectAll()
                .where('id', '=', Number(pathParams.id))
                .executeTakeFirst()

            if (user) {
                return createJsonResponse(user)
            } else {
                return {
                    statusCode: 404,
                    body: 'User not found',
                }
            }
        },
    },
    {
        method: 'POST',
        pathPattern: '/users',
        handle: async ({ jsonBody }) => {
            // TODO validate jsonBody
            const user = jsonBody as UserInsert

            await db.insertInto('users').values(user).execute()

            // TODO: return inserted id
            return createJsonResponse('OK')
        },
    },
    {
        method: 'GET',
        pathPattern: '/bundle.js',
        handle: async () => {
            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/javascript',
                },
                body: `console.log('TODO: bundle client code')`,
            }
        },
    },
    {
        handle: async ({ req, url }) => ({
            statusCode: 200,
            body: send(req, url.pathname, { root: srcClientStaticDir }),
        }),
    },
]
