import type { IncomingMessage } from 'node:http'

import type { Generated, Insertable, Selectable, Updateable } from 'kysely'
import type { SendStream } from 'send'

export interface RequestHandler {
    method?: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE'
    pathPattern?: string
    handle: (params: HandlerParams) => Promise<HandlerResponse>
}

export interface HandlerParams {
    req: IncomingMessage
    url: URL
    pathParams: Record<string, string>
    jsonBody?: unknown
}

export interface HandlerResponse {
    statusCode: number
    headers?: Record<string, string>
    body?: string | Uint8Array | SendStream
}

interface UsersTable {
    id: Generated<number>
    first_name: string
    last_name: string
}

export type UserSelect = Selectable<UsersTable>
export type UserInsert = Insertable<UsersTable>
export type UserUpdate = Updateable<UsersTable>

export interface Database {
    users: UsersTable
}
